﻿using System;
using System.Web.Http;
using ApiTestTd.MongoDb;

namespace ApiTestTd
{
    public abstract class BaseController<TModel> : ApiController
    {
        public bool TestEnvironment = false;

        /**
         * This method return the DAO instance.
         */
        protected IApiTestTdDao<TModel> DaoInstance()
        {
            return (IApiTestTdDao<TModel>) Singleton.Instance.DaoInstance[GetTypeLabel()];
        }

        /**
         * This method return the label of current type in string.
         */
        private string GetTypeLabel()
        {
            var key = typeof(TModel).Name.ToLower();
            return (TestEnvironment) ? key + "_test" : key;
        }

        /**
        * Get all elements
        *
        * Method:  GET
        * URL:     /{controller}/{id}
        */
        public IHttpActionResult Get()
        {
            return Ok(DaoInstance().FindAll());

        }

        /**
         * Get one element by ElementId.
         *
         * Method:  GET
         * URL:     /{controller}/{id}
         */
        public IHttpActionResult Get(int id)
        {
            return Ok(DaoInstance().FindOne(id));
        }

        /**
         * Insert one element
         *
         * Method:  POST
         * URL:     /{controller}
         */
        public IHttpActionResult Post(TModel model)
        {
            DaoInstance().InsertOne(model);
            return Ok();
        }

        /**
         * Update one element.
         *
         * Method:  PUT
         * URL:     /{controller}
         */
        public IHttpActionResult Put(TModel model)
        {
            DaoInstance().UpdateOne(model);
            return Ok();
        }

        /**
         * Remove one element by ElementId.
         *
         * Method:  DELETE
         * URL:     /{controller}/{id} 
         */
        public IHttpActionResult Delete(int id)
        {
            DaoInstance().DeleteOne(id);
            return Ok();
        }
    }
}
