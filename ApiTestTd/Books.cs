﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiTestTd
{
    public class Books
    {
        private int _Id;
        private string _Author;
        private string _Title;

        public int Id
        {
            get => _Id;
            set => _Id = value;
        }

        public string Author
        {
            get => _Author;
            set => _Author = value;
        }

        public string Title
        {
            get => _Title;
            set => _Title = value;
        }
    }
}
