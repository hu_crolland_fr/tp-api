﻿using ApiTestTd.Model;
using ApiTestTd.MongoDb;
using Microsoft.Owin.Hosting;
using System;

namespace ApiTestTd
{
    public class Program
    {
        private const string BaseUrl = "http://localhost:9000/";

        static void Main()
        {        
            // Init Singleton  
            Singleton.Instance.AddNewDaoInstance("book", new BookDao());
            Singleton.Instance.AddNewCollectionInstance("book", Singleton.Instance.DatabaseInstance().GetCollection<Book>("books"));

            // Start OWIN host 
            using (WebApp.Start<Startup>(url: BaseUrl))
            {
                Console.WriteLine(" Api Test TD");
                Console.WriteLine();
                Console.WriteLine(" Base URL : " + BaseUrl);
                Console.WriteLine(" Api URL : " + BaseUrl + "api/{controller}");
                Console.WriteLine(" Api Doc : " + BaseUrl + "swagger/ui/index");
                Console.WriteLine();
                Console.WriteLine(" App Running ...");
                Console.ReadLine();
            }
        }
    }
}