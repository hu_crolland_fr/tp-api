﻿using Owin;
using Swashbuckle.Application;
using System.Web.Http;

namespace ApiTestTd
{
    public class Startup
    {
        private const string Version = "v1";
        private const string Name = "Api Test TD";
        private const string RouteTemplate = "api/{controller}/{id}";

        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();

            config.EnableSwagger(c => c.SingleApiVersion(Version, Name)).EnableSwaggerUi();

            config.Routes.MapHttpRoute(
                name: Name,
                routeTemplate: RouteTemplate,
                defaults: new { id = RouteParameter.Optional }
            );

            appBuilder.UseWebApi(config);
        }
    }
}