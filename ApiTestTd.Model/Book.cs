﻿using MongoDB.Bson;

namespace ApiTestTd.Model
{
    public class Book : IApiTestTdModel
    {
        public ObjectId     Id { get; set; }
        public int          ElementId { get; set; }
        public string       Title { get; set; }
        public string       Description { get; set; }
        public Author       Author { get; set; }
    }
}

