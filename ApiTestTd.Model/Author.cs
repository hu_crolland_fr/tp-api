﻿using MongoDB.Bson;

namespace ApiTestTd.Model
{
    public class Author : IApiTestTdModel
    {
        public ObjectId  Id { get; set; }
        public int       ElementId { get; set; }
        public string    Name { get; set; }
    }
}
