﻿using MongoDB.Bson;

namespace ApiTestTd.Model
{
    public interface IApiTestTdModel
    {
        ObjectId  Id { get; set; }
        int       ElementId { set; get; }
    }
}
