﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace ApiTestTd.MongoDb
{
    public class Singleton
    {
        private static Singleton _instance;
        private static IMongoDatabase _database;
        private static IMongoClient _client; 
        public Dictionary<string, object> DaoInstance;
        public Dictionary<string, object> CollectionInstance;
      
        /**
         * This method add new DAO instance.
         */
        public void AddNewDaoInstance(string key, object dao)
        {
            if (DaoInstance == null)
            {
                DaoInstance = new Dictionary<string, object>();
            }

            DaoInstance.Add(key, dao);
        }

        /**
         * This method add new collection instance.
         */
        public void AddNewCollectionInstance(string key, object collection)
        {
            if (CollectionInstance == null)
            {
                CollectionInstance = new Dictionary<string, object>();
            }

            CollectionInstance.Add(key, collection);
        }

        /**
         * This method return the database instance.
         */
        public IMongoDatabase DatabaseInstance()
        {
            return _database ?? (_database = new DatabaseConnection().Database);
        }

        /**
         * This method return the client instance.
         */
        public IMongoClient ClientInstance()
        {
            return _client ?? (_client = new DatabaseConnection().Client);
        }

        /**
         * This method return the singleton instance.
         */
        public static Singleton Instance
        {
            get { return _instance ?? (_instance = new Singleton()); }
        }
    }
}
