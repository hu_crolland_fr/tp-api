﻿using ApiTestTd.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTestTd.MongoDb
{
    public class BookDao : IApiTestTdDao<Book>
    {
        /**
         * This method return the collection instance.
         */
        private IMongoCollection<Book> CollectionInstance()
        {
            return (IMongoCollection<Book>)Singleton.Instance.CollectionInstance["book"];
        }

        /**
         * This method return all books.
         */
        public async Task<List<Book>> FindAll()
        {
            return await CollectionInstance().Find(new BsonDocument()).ToListAsync();

        }

        /**
         * This method find book one by ElementId.
         */
        public async Task<Book> FindOne(int id)
        {
            var query = await CollectionInstance().Find(book => book.ElementId == id).ToListAsync();
            return query.FirstOrDefault();
        }

        /**
         * This method insert new book.
         */
        public async void InsertOne(Book book)
        {
            await CollectionInstance().InsertOneAsync(book);
        }

        /**
         * This method update one book by ElementId.
         */
        public async void UpdateOne(Book book)
        {
            await CollectionInstance().FindOneAndUpdateAsync(
                    Builders<Book>.Filter.Eq(b => b.ElementId, book.ElementId),
                    Builders<Book>.Update.Set(b => b.ElementId, book.ElementId)
                       .Set(b =>b.Title, book.Title)
                       .Set(b => b.Description, book.Description)
                       .Set(b => b.Author, book.Author)
                  );
        }

        /**
         * This method remove one book by ElementId
         */
        public async void DeleteOne(int id)
        {
            await CollectionInstance().DeleteOneAsync(book => book.ElementId == id);
        }
    }
}
