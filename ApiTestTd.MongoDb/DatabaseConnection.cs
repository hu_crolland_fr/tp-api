﻿using MongoDB.Driver;

namespace ApiTestTd.MongoDb
{
    public class DatabaseConnection
    {
        private const string Host = "mongodb://localhost:27017";
        public const string DbName = "ApiTestTd";
        public MongoClient Client;
        public IMongoDatabase Database;

        public DatabaseConnection()
        {
            Client = new MongoClient(Host);
            Database = Client.GetDatabase(DbName);
        }
    }
}
