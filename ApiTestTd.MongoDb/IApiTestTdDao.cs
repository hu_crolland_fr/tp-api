﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiTestTd.MongoDb
{
    public interface IApiTestTdDao<TModel>
    {
        Task<TModel> FindOne(int id);
        Task<List<TModel>> FindAll();
        void InsertOne(TModel model);
        void DeleteOne(int id);
        void UpdateOne(TModel model);
    }
}
