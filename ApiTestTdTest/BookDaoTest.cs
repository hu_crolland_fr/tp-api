﻿using System.Linq;
using System.Threading.Tasks;
using ApiTestTd.Model;
using ApiTestTd.MongoDb;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;

namespace ApiTestTdTest
{
    [TestFixture]
    public class BookDaoTest
    {
        private IMongoCollection<Book> _collection;

        [SetUp]
        public void SetUp()
        {
            _collection = Singleton.Instance.DatabaseInstance().GetCollection<Book>("books");
        }

        [TearDown]
        public void Dispose()
        {
            LoadDataFixtures.Execute();
            _collection = null;
        }

        [Test]
        public async Task FindAll()
        {
            var result = await _collection.Find(new BsonDocument()).ToListAsync();
            Assert.AreEqual(4, result.Count);
        }

        [Test]
        public async Task FindOne()
        {
            var result = await _collection.Find(book => book.ElementId == 1).ToListAsync();
            var currentBook = result.FirstOrDefault();
            Assert.AreEqual(1, currentBook.ElementId);
        }

        [Test]
        public void InsertOne()
        {
            Author author1 = new Author();
            author1.ElementId = 1;
            author1.Name = "Author Name 1";

            Book book = new Book();
            book.ElementId = 10;
            book.Title = "Book Title 10";
            book.Description = "Book Description 10";
            book.Author = author1;

            _collection.InsertOne(book);
            Assert.IsNotNull(book.Id);
        }
    }
}
