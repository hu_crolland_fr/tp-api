﻿using ApiTestTd.Model;
using ApiTestTd.MongoDb;
using MongoDB.Driver;

namespace ApiTestTdTest
{
    public static class LoadDataFixtures
    {
        public static void Execute()
        {
            Singleton.Instance.ClientInstance().DropDatabase("ApiTestTd");
            InsertBooks(Singleton.Instance.DatabaseInstance());
        }

        private static void InsertBooks(IMongoDatabase database)
        {
            Author author1 = new Author();
            author1.ElementId = 1;
            author1.Name = "Author Name 1";

            Author author2 = new Author();
            author2.ElementId = 2;
            author2.Name = "Author Name 2";

            // Book 1
            Book book1 = new Book();
            book1.ElementId = 1;
            book1.Title = "Book Title 1";
            book1.Description = "Book Description 1";
            book1.Author = author1;

            // Book 2
            Book book2 = new Book();
            book2.ElementId = 2;
            book2.Title = "Book Title 2";
            book2.Description = "Book Description 2";
            book2.Author = author1;

            // Book 3
            Book book3 = new Book();
            book3.ElementId = 3;
            book3.Title = "Book Title 3";
            book3.Description = "Book Description 3";
            book3.Author = author2;

            // Book 4
            Book book4 = new Book();
            book4.ElementId = 4;
            book4.Title = "Book Title 4";
            book4.Description = "Book Description 4";
            book4.Author = author2;

            var collection = database.GetCollection<Book>("books");

            collection.InsertOne(book1);
            collection.InsertOne(book2);
            collection.InsertOne(book3);
            collection.InsertOne(book4);
        }
    }
}
