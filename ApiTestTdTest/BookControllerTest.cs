﻿using ApiTestTd;
using ApiTestTd.Model;
using ApiTestTd.MongoDb;
using NSubstitute;
using NUnit.Framework;

namespace ApiTestTdTest
{
    [TestFixture]
    public class BookControllerTest
    {
        private IApiTestTdDao<Book> _daoInstance;
        private BookController _controller;
  
        [SetUp]
        public void SetUp()
        {
            var dao = Substitute.For<IApiTestTdDao<Book>>();
            Singleton.Instance.AddNewDaoInstance("book", dao);
            _daoInstance = dao;
            _controller = new BookController();
        }

        [TearDown]
        public void Dispose()
        {
            Singleton.Instance.DaoInstance.Clear();
            _controller = null;
        }

        [Test]
        public void TestFindAll()
        {
            _controller.Get();
            _daoInstance.Received(1).FindAll();
        }

        [Test]
        public void TestFindOne()
        {
            _controller.Get(1);
            _daoInstance.Received(1).FindOne(1);
        }

        [Test]
        public void TestInsertOne()
        {
            _controller.Post(new Book());
            _daoInstance.Received(1).InsertOne(Arg.Any<Book>());
        }

        [Test]
        public void TestUpdateOne()
        {
            _controller.Put(new Book());
            _daoInstance.Received(1).UpdateOne(Arg.Any<Book>());
        }

        [Test]
        public void TestDeleteOne()
        {
            _controller.Delete(1);
            _daoInstance.Received(1).DeleteOne(1);
        }
    }
}
